package com.finalcustomer.projectcustomer;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.finalcustomer.entity.IndiaStateEntity;

public interface IndiaStateRepo extends CrudRepository<IndiaStateEntity, Integer>{
	public ArrayList<IndiaStateEntity> findAll();


}
