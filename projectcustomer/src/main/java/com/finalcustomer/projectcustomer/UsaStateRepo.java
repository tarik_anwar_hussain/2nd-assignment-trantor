package com.finalcustomer.projectcustomer;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.finalcustomer.entity.UsaStateEntity;

public interface UsaStateRepo extends CrudRepository<UsaStateEntity, Integer>{
	public ArrayList<UsaStateEntity> findAll();

}
