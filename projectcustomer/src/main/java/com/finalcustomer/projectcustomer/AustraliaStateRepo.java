package com.finalcustomer.projectcustomer;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.finalcustomer.entity.AustraliaStateEntity;

public interface AustraliaStateRepo extends CrudRepository<AustraliaStateEntity, Integer>{
	public ArrayList<AustraliaStateEntity> findAll();

}
