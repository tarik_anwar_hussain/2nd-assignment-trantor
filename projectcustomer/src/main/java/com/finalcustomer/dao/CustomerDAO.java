package com.finalcustomer.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finalcustomer.entity.AustraliaStateEntity;
import com.finalcustomer.entity.CanadaStateEntity;
import com.finalcustomer.entity.CountryEntity;
import com.finalcustomer.entity.CustomerEntity;
import com.finalcustomer.entity.IndiaStateEntity;
import com.finalcustomer.entity.UsaStateEntity;
import com.finalcustomer.projectcustomer.AustraliaStateRepo;
import com.finalcustomer.projectcustomer.CanadaStateRepo;
import com.finalcustomer.projectcustomer.CountryRepo;
import com.finalcustomer.projectcustomer.CustomerRepository;
import com.finalcustomer.projectcustomer.IndiaStateRepo;
import com.finalcustomer.projectcustomer.UsaStateRepo;

@Repository
public class CustomerDAO {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CountryRepo countryRepo;
//
//	@Autowired
//	private StateRepo stateRepo;

	@Autowired
	private AustraliaStateRepo australiaStateRepo;

	@Autowired
	private IndiaStateRepo indiaStateRepo;

	@Autowired
	private UsaStateRepo usaStateRepo;

	@Autowired
	private CanadaStateRepo canadaStateRepo;

	@Transactional
	public ArrayList<UsaStateEntity> getUsaStates() {
		return usaStateRepo.findAll();
		 
	}
	
	@Transactional
	public ArrayList<IndiaStateEntity> getIndiaStates() {
		return indiaStateRepo.findAll();
	}
	
	@Transactional
	public ArrayList<CanadaStateEntity> getCanadaStates() {
		return canadaStateRepo.findAll();
	}
	
	@Transactional
	public ArrayList<AustraliaStateEntity> getAustraliaStates() {
		return australiaStateRepo.findAll();
	}

		
	
	@Transactional
	public void addCustomer(CustomerEntity customer) {
		customerRepository.save(customer);

	}

	@Transactional
	public void removeCustomer(CustomerEntity customer) {
		customerRepository.delete(customer);
	}

	@Transactional
	public ArrayList<CustomerEntity> searchCustomerById(Integer id) {

		return customerRepository.findByCustomerId(id);
	}

	@Transactional
	public CustomerEntity searchbycust(Integer id) {
		CustomerEntity entity = customerRepository.findByCustId(id);
		return entity;

	}

	@Transactional
	public ArrayList<CustomerEntity> searchCustomer() {

		return customerRepository.findAll();

	}

	@Transactional
	public void updateCustomer(CustomerEntity customer) {
		customerRepository.save(customer);
	}

	@Transactional
	public ArrayList<CountryEntity> getCountry() {
		ArrayList<CountryEntity> employeeList = countryRepo.findAll();
		return employeeList;
	}

//	@Transactional
//	public ArrayList<StateEntity> getState() {
//		return stateRepo.findAll();
//	}

}
